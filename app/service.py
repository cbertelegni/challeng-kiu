from app.store import packages_store, customers_store
from random import randint
from datetime import datetime


class NoCustomersException(Exception):
    ...


class PaymentException(Exception):
    ...


class PymentService:
    def post(self, *args, **kwargs):
        # calling to payment_service
        response = {'status': 'success', 'message': 'Payment ok'}
        return response

    def do_payment(self, customer_id: str, price: float):
        response = self.post(customer_id, price)
        if response['status'] != 'success':
            raise PaymentException(response['message'])


class PackageService:
    def __init__(self, payment_service: PymentService, price: float = 10):
        self.package_price = price
        self.payment_service = payment_service

    @classmethod
    def create(cls, **kwargs):
        payment_service = PymentService()
        return cls(payment_service=payment_service, **kwargs)

    @staticmethod
    def check_is_customer(customer_id: int):
        if not customers_store.find(customer_id):
            raise NoCustomersException

    def charge_package(self, customer_id: str, price: float):
        self.payment_service.do_payment(customer_id, price)

    def _track_package(self, customer_id: str, source:str, target: str, date: datetime = datetime.now()):
        package_id = randint(1, 100000)
        package = {
            'id': package_id,
            'source': source,
            'target': target,
            'customer_id': customer_id,
            'price': self.package_price,
            'date': date
        }
        packages_store.push(package)
        return package

    def send_package(self, customer_id: str, source:str, target: str, date: datetime = None):
        self.check_is_customer(customer_id)
        self.charge_package(
            customer_id, self.package_price
        )
        package = self._track_package(customer_id, source, target, date)
        return package

    def get_packages_by_date(self, date: datetime) -> list:
        packages = [
            package for package in packages_store.items if
            package['date'].replace(hour=0, minute=0, second=0, microsecond=0) == date
        ]
        return packages

    def get_report_by_date(self, date: datetime) -> dict:
        packages = self.get_packages_by_date(date)
        return {
            'total_packages': len(packages),
            'total_price': len(packages) * 10
        }