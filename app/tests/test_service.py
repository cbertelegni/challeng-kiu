from unittest.mock import patch
from datetime import datetime
import unittest
from app.service import PackageService, NoCustomersException, PaymentException
from app.store import packages_store, customers_store


class TestService(unittest.TestCase):
    def setUp(self):
        valid_customers = [
            {'id':1, 'name': '<NAME> 1'}
        ]
        customers_store.push(*valid_customers)

    @classmethod
    def tearDownClass(cls):
        customers_store.reset()
        packages_store.reset()

    def test_no_customer_package_must_fail(self):
        service = PackageService.create()
        with self.assertRaises(NoCustomersException, msg="Must raise NoCustomersException if the customer doesn't exist"):
            service.send_package(
                customer_id=333,
                source='Buenos Aires',
                target='Mar del Plata'
            )

    @patch('app.service.PymentService.post')
    def test_the_payment_must_raise_if_response_is_not_successful(self, mock_post_pyment_service):
        mock_post_pyment_service.return_value = {'status': 'error', 'message': 'error'}
        service = PackageService.create()
        with self.assertRaises(PaymentException, msg='Must raise if payment response is not successful'):
            service.send_package(
                customer_id=1,
                source='Buenos Aires',
                target='Mar del Plata'
            )

    def test_package_must_be_save_on_store(self):
        service = PackageService.create()
        package = service.send_package(
            customer_id=1,
            source='Buenos Aires',
            target='Mar del Plata'
        )
        self.assertIsNotNone(packages_store.find(package['id']), msg='The package should have been saved')

    def test_get_packages_report(self):
        packages_store.reset()
        service = PackageService.create()
        for day in range(1, 6):
            # append days between 1/2/2024 to 5/2/2014
            for _ in range(10):
                service.send_package(
                    customer_id=1,
                    source='Buenos Aires',
                    target='Mar del Plata',
                    date=datetime(day=day, month=2, year=2024)
                )
        self.assertEqual(len(packages_store.items), 50)
        report = service.get_report_by_date(date=datetime(year=2024, month=2, day=1))
        self.assertEqual(report['total_packages'], 10, msg='The total packages report should be 10')
        self.assertEqual(report['total_price'], 100, msg='The total price report should be 100')

        report = service.get_report_by_date(date=datetime(year=2024, month=5, day=11))
        self.assertEqual(report['total_packages'], 0, msg='The total packages report should be 0')
        self.assertEqual(report['total_price'], 0, msg='The total price report should be 0')
