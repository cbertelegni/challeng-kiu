import unittest
from app.store import Store


class TestStore(unittest.TestCase):

    def test_push_store(self):
        store = Store('test_store')
        self.assertEqual(len(store.items), 0)

        store.push({
            'id': 1,
            'name': 'Test push'
        })
        self.assertEqual(len(store.items), 1)

    def test_find_store(self):
        store = Store('test_store')
        item_1 = {'id':1, 'name': 'item 1'}
        store.push(item_1, {'id':2, 'name': 'item 2'})
        self.assertEqual(len(store.items), 2)
        item = store.find(1)
        self.assertEqual(item, item_1)

    def test_reset_store(self):
        store = Store('test_store')
        items = [{'id':1, 'name': 'item 1'}, {'id':2, 'name': 'item 2'}]
        store.push(*items)
        self.assertEqual(len(store.items), 2)
        store.reset()
        self.assertEqual(len(store.items), 0, msg='The store must be 0 after reset it')
