class Store:
    def __init__(self, name: str):
        self.name = name
        self.reset()

    def push(self, *items: dict):
        for item in items:
            self._items[item["id"]] = item

    def find(self, id: int) -> dict:
        return self._items.get(id)

    def reset(self):
        self._items = dict()

    @property
    def items(self) -> list:
        return list(self._items.values())


packages_store = Store("packages")
customers_store = Store("customers")
